RSS field formatters
====================

Provides field formatters for node RSS view mode:

  * `<category>` element for taxonomy term reference fields;
  * `<enclosure>` element for file, image and media fields (RSS enclosures are
    the method used to publish MP3 files in a podcast feed);
  * `<dc:date>` and `<pubDate>` elements for date fields;
  * `<dc:creator>` element for text and user reference fields; and
  * `<georss:point>` element for geofields and geolocation fields.

These field formatters restore support for RSS category and enclosure elements
that disappeared with Drupal 7. Unlike previous versions of Drupal which
rendered the RSS elements automatically, they must now be manually enabled by
configuring the display settings for RSS view mode at admin/structure/types and
selecting RSS category or RSS enclosure as the format.

## To use these field formatters in a view:

In the content type, under manage display, enable custom display settings for
RSS, save, then click the RSS tab and select RSS formats for your fields as
appropriate. In the view, select Format: RSS Feed, Show: Content and Display
Type: RSS. You can of course use features module to export your content type and
view configuration to code.

## Maintainers:

  * [mfb](https://www.drupal.org/u/mfb)

## Supporting organizations:

  * [EFF](https://www.drupal.org/node/2368685)
